import React from 'react';
import { PostsService } from '../../services/posts.service';

export class PostDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            post: null,
        }
    }

    componentDidMount() {
        const id = this.props.match.params.id;

        PostsService.fetchPost(id)
            .then(post => this.setState({ post }));
    }

    render() {
        if (!this.state.post) {
            return null;
        }

        const { title, body } = this.state.post;

        return (
            <article>
                <h1>{title}</h1>

                <div>{body}</div>
            </article>
        );
    }
}