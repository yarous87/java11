import React from 'react';
import { Link } from 'react-router-dom';
import { PostsService } from '../../services/posts.service';

import './Admin.scss';

export class Admin extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            posts: [],
        }
    }

    fetchPosts() {
        return PostsService.fetchPosts()
            .then(posts => this.setState({ posts }));
    }

    componentDidMount() {
        this.fetchPosts();
    }

    handleDelete(postId) {
        const c = confirm('Czy na pewno chcesz kontynuować?');

        if (!c) {
            return;
        }

        PostsService.deletePost(postId)
            .then(this.fetchPosts());
    }

    render() {
        return (
            <table className="Admin">
                <tbody>
                    {this.state.posts.map(post => (
                        <tr key={post.id}>
                            <td>{post.id}</td>
                            <td>{post.title}</td>
                            <td>
                                <Link to={`/admin/edit/${post.id}`}>Edytuj</Link>
                                <button onClick={() => this.handleDelete(post.id)}>
                                    Usuń
                                </button>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        )
    }
}