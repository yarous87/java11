import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import { Header } from '../Header/Header';
import { Footer } from '../Footer/Footer';
import { PostList } from '../PostList/PostList';

import './App.scss';
import { PostDetails } from '../PostDetails/PostDetails';
import { Admin } from '../Admin/Admin';
import { PostCreate } from '../PostCreate/PostCreate';
import { PostEdit } from '../PostEdit/PostEdit';
// window.axios = axios; // udostepnienie zmienej globalnie, np do wykorzystania w konsoli, nie robic na produkcji

export class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <div className="App">
                    <main>
                        <Header />
                        
                        
                        <Switch>
                            <Route path="/" exact component={PostList} />
                            <Route path="/posts/:id" component={PostDetails} />
                            <Route path="/admin" exact component={Admin} />
                            <Route path="/admin/create" component={PostCreate} />
                            <Route path="/admin/edit/:id" component={PostEdit} />
                        </Switch>
                    </main>

                    <Footer />
                </div>
            </BrowserRouter>
        )
    }
}