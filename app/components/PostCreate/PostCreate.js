import React from 'react';
import { PostsService } from '../../services/posts.service';
import { PostForm } from '../PostForm/PostForm';

export class PostCreate extends React.Component {
    createPost = (data) => {
        return PostsService.createPost(data);
    }

    render() {
        return (
            <PostForm onSubmit={this.createPost} />
        )
    }
}