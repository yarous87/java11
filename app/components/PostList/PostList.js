import React from 'react';
import axios from 'axios';
import { PostListItem } from '../PostListItem/PostListItem';
import { PostsService } from '../../services/posts.service';

export class PostList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            posts: [],
        }
    }

    componentDidMount() {
        PostsService.fetchPosts()
            .then(posts => this.setState({ posts }));
    }

    render() {
        return (
            <>
                {this.state.posts.map(post => <PostListItem key={post.id} post={post} />)}
            </>
        );
    }
}