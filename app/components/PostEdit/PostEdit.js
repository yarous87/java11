import React from 'react';
import { PostsService } from '../../services/posts.service';
import { PostForm } from '../PostForm/PostForm';

export class PostEdit extends React.Component {
    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount() {
        const id = this.props.match.params.id;

        PostsService.fetchPost(id)
            .then(post => this.setState({ post }));
    }

    updatePost = (data) => {
        const id = this.props.match.params.id;
        
        return PostsService.updatePost(
            id,
            data,
        );
    }

    render() {
        const post = this.state.post;

        if (!post) return 'Loading ...';

        return (
            <PostForm
                initialState={post}
                onSubmit={this.updatePost} 
            />
        );
    }
}