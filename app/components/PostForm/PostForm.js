import React from 'react';

function wait(time) {
    return new Promise(
        (resolve, reject) => setTimeout(resolve, time)
    )
}

export class PostForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            post: props.initialState || {
                title: '',
                body: '',
            },
            status: 'DEFAULT', // SUCCESS | ERROR
            emptyFields: [],
            pristineFields: props.initialState ? [] : ['title', 'body'],
        }
    }

    handleInputChange = (event) => {
        const { name, value } = event.target;
        const { emptyFields, pristineFields } = this.state;

        let idx = emptyFields.indexOf(name);

        if (value && idx !== -1) {
            emptyFields.splice(idx, 1);
        } else if (!value) {
            emptyFields.push(name);
        }

        idx = pristineFields.indexOf(name);

        if (idx !== -1) {
            pristineFields.splice(idx, 1);
        }

        this.setState({ 
            post: {
                ...this.state.post,
                [name]: value,
            },
            emptyFields,
            pristineFields,
        });
    }

    handleFormSubmit = (event) => {
        event.preventDefault();

        this.setState({ status: 'SUBMITTING' });

        this.props.onSubmit(this.state.post)
            .then(() => wait(3000))
            .then(() => this.setState({ status: 'SUCCESS' }))
            .catch(() => this.setState({ status: 'ERROR' }));
    }

    render() {
        const { title, body } = this.state.post;
        const { status, emptyFields, pristineFields } = this.state;
        const isDisabled = status === 'SUBMITTING' || emptyFields.length || pristineFields.length;

        return (
            <form onSubmit={this.handleFormSubmit} method="post">
                {status === 'SUCCESS' && (
                    <div>Dane zostały zapisane</div>
                )}
                {status === 'ERROR' && (
                    <div>Wystąpił błąd spróbuj ponownie.</div>
                )}
                <div>
                    <label>Tytuł</label><br />
                    <input name="title" value={title} onChange={this.handleInputChange}  />
                    {emptyFields.indexOf('title') !== -1 && <p>Pole wymagane</p>}
                </div>

                <div>
                    <label>Treść</label><br />
                    <textarea name="body" value={body} onChange={this.handleInputChange} />
                    {emptyFields.indexOf('body') !== -1 && <p>Pole wymagane</p>}
                </div>

                <button type="submit" disabled={isDisabled}>Wyślij</button>
            </form>
        );
    }
}