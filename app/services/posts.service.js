import { HttpService } from "./http.service";

const POSTS_URL = 'http://localhost:3000/posts';

class PostsService {
    fetchPosts() {
        return HttpService.get(POSTS_URL)
            .then(resp => resp.data);
    }

    fetchPost(postId) {
        return HttpService.get(`${POSTS_URL}/${postId}`)
            .then(resp => resp.data);
    }

    createPost(data) {
        return HttpService.post(
            POSTS_URL,
            data
        )
    }

    updatePost(postId, data) {
        return HttpService.put(
            `${POSTS_URL}/${postId}`,
            data,
        )
    }

    deletePost(postId) {
        return HttpService.delete(`${POSTS_URL}/${postId}`)
    }
}

const PostsServiceInstance = new PostsService();

export {
    PostsServiceInstance as PostsService
}