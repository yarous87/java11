import axios from 'axios';

class HttpService {
    get(url) {
        return axios.get(url);
    }

    post(url, data) {
        return axios.post(url, data);
    }

    patch(url, data) {
        return axios.patch(url, data);
    }

    put(url, data) {
        return axios.put(url, data);
    }

    delete(url) {
        return axios.delete(url);
    }
}

const HttpServiceInstance = new HttpService();

export {
    HttpServiceInstance as HttpService
}